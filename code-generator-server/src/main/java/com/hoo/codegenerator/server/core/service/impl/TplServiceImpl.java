package com.hoo.codegenerator.server.core.service.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.hoo.codegenerator.server.core.bean.dto.TplDto;
import com.hoo.codegenerator.server.core.service.TplService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

/**
 * 模版服务实现类
 *
 * @author hank
 * @create 2020-12-29 上午10:27
 **/
@Service
public class TplServiceImpl implements TplService {
    @Autowired JdbcTemplate jdbcTemplate;

    @Override
    public Object queryAll() {
        return jdbcTemplate.query("SELECT a.title,a.content,a.suffix,a.tpl_id,a.group_id,b.group_name FROM T_CODEGEN_TPL a LEFT JOIN T_CODEGEN_TPL_GROUP b ON a.group_id = b.group_id", new BeanPropertyRowMapper(TplDto.class));
    }

}
